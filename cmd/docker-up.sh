#!/bin/bash

# --------------------------------------------------------------------------
# CMD: Start docker containers
# @author Hervé Perchec <herve.perchec@gmail.com>
# --------------------------------------------------------------------------

# Get directory of 'app' parent folder and move into
SCRIPT_DIR=$(dirname "${BASH_SOURCE[0]}")
cd $SCRIPT_DIR/../
APP_DIR=$PWD

# Show header function
show_header() {
    echo -e "\e[36m-------------------------------------------------------------\e[0m"
    echo -e "\e[36mCMD: Start docker containers\e[0m"
    echo -e "\e[36mAUTHOR: Hervé Perchec <herve.perchec@gmail.com>\e[0m"
    echo -e "\e[36m-------------------------------------------------------------\e[0m"
    echo
}

# --------------------------------------------------------------------------
# END INIT
# --------------------------------------------------------------------------

# Show header
show_header

# --------------------------------------------------------------------------
# Step 1: Prevent using of 'UI' container if Windows environment, due to loss of performance
# --------------------------------------------------------------------------

# Define if Windows environment
IS_WINDOWS=0

# Detect OS
echo -e "\e[36mRead environment variable OSTYPE:\e[0m $OSTYPE"
echo
case "$OSTYPE" in
  msys*)
    echo -e "\e[36mDetected OS:\e[0m WINDOWS"
    IS_WINDOWS=1
    ;;
  cygwin*)
    echo -e "\e[36mDetected OS:\e[0m WINDOWS (cygwin)"
    IS_WINDOWS=1
    ;;
  *)
    echo -e "\e[36mDetected OS:\e[0m $OSTYPE"
    ;;
esac

echo

# If windows -> display warning message and ask for UI container launching
if [ $IS_WINDOWS -eq 1 ]; then
  echo -e "\e[33;1mWARNING:\e[0m"
  echo
  echo -e "\e[33mWindows OS detected! Using docker container in \e[1;4mdevelopment\e[0m \e[33mfor 'UI' subproject is not optimized.\e[0m"
  echo
  echo -e "\e[33mWebpack-dev-server hot reload is extremely slow when it used without \e[1mcached volume\e[0m \e[33mdue to the size of the node_modules folder.\e[0m"
  echo
  echo -e "\e[33mUnfortunately, Windows does not support \e[1mcached volume\e[0m \e[33m...\e[0m"
  echo -e "\e[33mPray for Windows improvements in the future ¯\_(ツ)_/¯\e[0m"
  echo
  echo -e "\e[33mThere seems to be a workaround as explained in this tutorial: \e[4mhttps://www.youtube.com/watch?v=dQw4w9WgXcQ\e[0m"
  echo
  echo -e "\e[33mSee official Docker documentation: \e[4mhttps://docs.docker.com/storage/bind-mounts/#configure-mount-consistency-for-macos\e[0m"
  echo
  echo -e "\e[33mSo, due to this issue, the launching of the 'UI' container will be skipped.\e[0m"
  echo

  # While var is not set (While correct reply is not given)
  while [ -z ${SKIP_UI_CONTAINER_LAUNCHING+y} ]; do
    # Prompt
    read -e -p $'\e[36mSkip \'UI\' container launching? (Yes/No) \e[0m' -i "Yes"
    # Allow "yes" or "no" (case insensitive)
    case "$REPLY" in 
      [yY][eE][sS] )
        SKIP_UI_CONTAINER_LAUNCHING=1
        ;;
      [nN][oO] )
        SKIP_UI_CONTAINER_LAUNCHING=0
        ;;
      * )
        echo -e "\e[31mInvalid choice\e[0m"
        ;;
    esac
  done

fi

# If SKIP_UI_CONTAINER_LAUNCHING = 1 -> run only 'server' and 'database' services
if [ $SKIP_UI_CONTAINER_LAUNCHING -eq 1 ]; then
  docker-compose --env-file $APP_DIR/.docker/.env up -d database server
else
  docker-compose --env-file $APP_DIR/.docker/.env up -d
fi
